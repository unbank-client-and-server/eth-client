/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import './shim.js';
import crypto from 'crypto';
import hdkey from "ethereumjs-wallet/hdkey";
import bip39 from "react-native-bip39";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

  constructor(props: Props) {
    super(props);
    this.state = {
      mnemonic: [],
      seed: []
    };
  }


  componentDidMount() {
    const _self = this;

    bip39.generateMnemonic(256).then(function(mnemonic) {
      // You can generate a mnemonic seed with bip39 library
      // const mnemonic = "seed sock milk ...";
      const path = "m/44'/60'/0'/0/0";

      const hdwallet = hdkey.fromMasterSeed(bip39.mnemonicToSeed("muscle skate dawn remember pumpkin foil vacuum such brass grass bullet shoulder"));
      const wallet = hdwallet.derivePath(path).getWallet();
      const address = "0x" + wallet.getAddress().toString("hex");
      const publicKey = "0x" + wallet.getPublicKeyString().toString("hex");

      console.warn("address : " + address);

      console.warn("publicKey : " + publicKey);

    }).catch(function(error) {
      console.warn('There has been a problem with your fetch operation: ' + error.message);
      // ADD THIS THROW error
      throw error;
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
