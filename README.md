# ETH client

React Native repository for ETH lib and transaction (WIP)

## Libraries used

- [react-native-crypto][lib1] - React Native alternative from crypto library
- [ethereumjs-wallet/hdkey][lib2] - Transactions and getting public / private key + address
- [react-native-bip39][lib3] - Mnemonic

## Setup

### Install libraries

```
npm i –save react-native-crypto
npm i –save ethereumjs-wallet
npm i –save react-native-bip39
```

### If BASE_MAP.fill(255) error

- go in node-modules/base-x/index.js
- change : //BASE_MAP.fill(255)
- insert this :

```
for (let i = 0; i < 256; i++) {
    BASE_MAP[i] = 255
}
```

## Usage (get from a specified path)

- ``` bip39.generateMnemonic(256) ```
Is generating a random seed (24 words). The method is asynchronous

- ``` path = "m/44'/60'/0'/0/0" ```
defines the specific path and coin

- ``` wallet = hdwallet.derivePath(path).getWallet() ```
get the wallet from the chosen path

- ``` publicKey = "0x" + wallet.getPublicKeyString().toString("hex") ```
gives the public key

- ``` address = "0x" + wallet.getAddress().toString("hex") ```
gives the address

- ``` getWallet().getPrivateKeyString() ```
gives the private key


[lib1]: https://github.com/tradle/react-native-crypto
[lib2]: https://github.com/ethereumjs/ethereumjs-wallet
[lib3]: https://github.com/novalabio/react-native-bip39